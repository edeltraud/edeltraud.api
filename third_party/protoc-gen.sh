#!/usr/bin/env bash
protoc --proto_path=edeltraud.api/proto/v1 --proto_path=edeltraud.api/third_party --go_out=plugins=grpc:edeltraud.api/pkg/api/v1/go user-service.proto course-service.proto
